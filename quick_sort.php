<?php
/*
 * Тут подразумевается что неймспейсы могут быть, но упустил этот момент, т.к. показалаось, что тестовое задание
 * подразумевает проверку другого рода экспертиз
 */
class QuickSorter
{
    /**
     * Реализация алгоритма быстрой сортировк
     * @param array $array
     * @param int|null $suggestedPivot
     */
    public function sort(array &$array, ?int $suggestedPivot = null)
    {
        $initialLeftBorder = 0;
        $initialRightBorder = \count($array) - 1;

        $this->q_sort($array, $initialLeftBorder, $initialRightBorder, $suggestedPivot);
    }

    /**
     * Непосредственно сортировка
     * @param array $array
     * @param int $leftBorder
     * @param int $rightBorder
     * @param int|null $suggestedPivot
     */
    private function q_sort(array &$array, int $leftBorder, int $rightBorder, ?int $suggestedPivot = null)
    {
        $leftIteratorPosition = $leftBorder;
        $rightIteratorPosition = $rightBorder;

        $pivot = $suggestedPivot ?? $array[(int)($leftBorder + $rightBorder) / 2];

        do {
            while ($array[$rightIteratorPosition] > $pivot) {
                $rightIteratorPosition--;
            }

            while ($array[$leftIteratorPosition] < $pivot) {
                $leftIteratorPosition++;
            }

            if ($leftIteratorPosition <= $rightIteratorPosition) {

                list(
                    $array[$rightIteratorPosition],
                    $array[$leftIteratorPosition]
                    ) = array(
                        $array[$leftIteratorPosition],
                        $array[$rightIteratorPosition])
                ;

                $leftIteratorPosition++;
                $rightIteratorPosition--;
            }

        } while ($leftIteratorPosition <= $rightIteratorPosition);

        if ($rightIteratorPosition > $leftBorder) {
            $this->q_sort($array, $leftBorder, $rightIteratorPosition);
        }

        if ($leftIteratorPosition < $rightBorder) {
            $this->q_sort($array, $leftIteratorPosition, $rightBorder);
        }
    }
}

class ArrayMerger
{
    /**
     * Слияние двух массивов. Ключи не учитываются. Дублирующиеся элементы опускаются.
     * К концу первого массива $array1 дописываются элементы массива $array2
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public function merge(array $array1, array $array2): array
    {
        $result = [];

        foreach ($array1 as $element){
            $result[] = $element;
        }

        foreach ($array2 as $element){
            if(!\in_array($element,$result)){
                $result[] = $element;
            }
        }

        return $result;
    }
}

class QuickSortPivotSuggester
{
    /**
     * Вычисляет опорный элемент для сортировки двух неубывающих массивов
     * @param array $array1
     * @param array $array2
     * @return int
     */
    public function suggest(array $array1, array $array2): int
    {
        $array1Length = \count($array1) - 1;
        $array2Length = \count($array2) - 1;
        $array1MinElement = $array1[0];
        $array2MinElement = $array2[0];
        $array1MaxElement = $array1[$array1Length];
        $array2MaxElement = $array2[$array2Length];
        $absoluteMin = $array1MinElement < $array2MinElement ? $array1MinElement : $array2MinElement;
        $absoluteMax = $array1MaxElement > $array2MaxElement ? $array1MaxElement : $array2MaxElement;
        $suggestedPivot = ($absoluteMin + $absoluteMax) / 2;

        return $suggestedPivot;
    }
}

class TestTaskOne
{
    public function doTestTask()
    {
        $arrayMerger = new ArrayMerger();
        $quickSorter = new QuickSorter();
        $pivotSuggester = new QuickSortPivotSuggester();
        $array1 = [100, 500, 666, 999, 10500, 100500, 777777];
        $array2 = [1, 2, 69, 300, 600, 900, 999, 777777];
        $suggestedPivot = $pivotSuggester->suggest($array1,$array2);

        $array = $arrayMerger->merge($array1,$array2);
        print_r($array);

        $quickSorter->sort($array,$suggestedPivot);
        print_r($array);
    }
}

$testTaskOne = new TestTaskOne();
$testTaskOne->doTestTask();