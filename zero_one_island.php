<?php

class IslandLocator
{

    /**
     * Получение количества островков из единичек в море нулей
     * Островом считается сама единичка, либо совокупность единичек по горизонтали и вертикали (диагональ не в счет)
     *
     * @param array $islandsMatrix
     * @param int $rows
     * @param int $cols
     * @return int
     */
    public function getIslandsCount(array $islandsMatrix, int $rows, int $cols): int
    {
        $islandsOnMatrix = 0;

        foreach ($islandsMatrix as $rowKey => $row){
            foreach ($row as $colKey => $value){
                if($this->isIslandAtPosition($islandsMatrix, $rowKey, $colKey)){
                    $islandsOnMatrix++;
                }
            }
        }

        return $islandsOnMatrix;
    }

    /**
     * Определяет, есть ли остров в текущей позиции
     * @param array $islandsMatrix
     * @param int $rows
     * @param int $cols
     * @return bool
     */
    private function isIslandAtPosition(array &$islandsMatrix, int $rows, int $cols): bool
    {
        if (!isset($islandsMatrix[$rows][$cols])) {
            return false;
        }

        $isIslandAtPosition = 1 === $islandsMatrix[$rows][$cols];
        $islandsMatrix[$rows][$cols] = 0;

        if ($isIslandAtPosition) {
            $this->isIslandAtPosition($islandsMatrix, $rows, $cols + 1);
            $this->isIslandAtPosition($islandsMatrix, $rows, $cols - 1);
            $this->isIslandAtPosition($islandsMatrix, $rows + 1, $cols);
            $this->isIslandAtPosition($islandsMatrix, $rows - 1, $cols);
        }

        return $isIslandAtPosition;
    }
}

class TestTaskTwo
{
    public function doTestTask()
    {
        $isladnsMatrix = [
            [0, 0, 0, 0, 1,],
            [0, 0, 1, 0, 0,],
            [1, 1, 0, 1, 1,],
            [1, 1, 0, 1, 0,],
        ];
        $rows = 4;
        $cols = 5;

        $islandLocator = new IslandLocator();
        echo 'Given matrix has ' . $islandLocator->getIslandsCount($isladnsMatrix, $rows, $cols) . ' islands';
    }
}

$testTaskTwo = new TestTaskTwo();
$testTaskTwo->doTestTask();